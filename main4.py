import sqlite3
import hashlib
from tkinter import *
from tkinter import simpledialog
from functools import partial
import os

import uuid
from threading import Timer

# runs decryption method on password_vault.db
os.system("decryptionMethod.py")

# database code
with sqlite3.connect("password_vault.db") as db:
    cursor = db.cursor()
# creates a table for masterpassword if one doesn't already exist
cursor.execute("""
CREATE TABLE IF NOT EXISTS masterpassword(
id INTEGER PRIMARY KEY,
password TEXT NOT NULL);
""")
# Table which stores various details such as email
cursor.execute("""
CREATE TABLE IF NOT EXISTS details(
id INTEGER PRIMARY KEY,
email TEXT NOT NULL);
""")
# creates columns for username, website, and password entries within vault table
cursor.execute("""
CREATE TABLE IF NOT EXISTS vault(
id INTEGER PRIMARY KEY,
username TEXT NOT NULL,
website TEXT NOT NULL,
password TEXT NOT NULL);
""")





# create a popup
def popUp(text):
    answer = simpledialog.askstring("input String", text)

    return answer


# Intitialize window
window = Tk()
window.title("password vault")


#   hashing function - add salt
def hashPassword(input):
    salt = uuid.uuid4().hex
    hash = hashlib.sha256(input)
    hash = hash.hexdigest()
    return hash


# registration screen
def firstScreen():
    window.geometry("360x260")  # screen size

    lbl = Label(window, text="create master password:")
    lbl.config(anchor=CENTER)
    lbl.pack()
    # user input of master password
    txt = Entry(window, width=20, show="*")

    txt.pack()
    txt.focus()

    # label for password confirmation
    lbl1 = Label(window, text="Re-Enter password:")
    lbl1.pack()

    txt1 = Entry(window, width=20, show="*")
    txt1.pack()
    txt1.focus()

    # secret Entry
    lbl3 = Label(window, text="Secret:")
    lbl3.config(anchor=CENTER)
    lbl3.pack()
    # user input of secret
    txt2 = Entry(window, width=20)
    txt2.pack()
    txt2.focus()
    # email entry
    lbl4 = Label(window, text="Email:")
    lbl4.config(anchor=CENTER)
    lbl4.pack()
    # user input of email
    txt3 = Entry(window, width=20)
    txt3.pack()
    txt3.focus()
    # name entry
    lbl4 = Label(window, text="Name:")
    lbl4.config(anchor=CENTER)
    lbl4.pack()
    # user input of name
    txt4 = Entry(window, width=20)
    txt4.pack()
    txt4.focus()

    lbl2 = Label(window)
    lbl2.pack()

    lbl3

    def savePassword():
        os.system("Email.py")

        SpecialSym = ['$', '@', '#', '%']
       # if the registered password is the same as the confirmation password & the length is more than 6, save the password to the db
        if txt.get() == txt1.get():
            if len(txt.get()) >= 6:

                # encode password for hashing
                hashedPassword = hashPassword(txt.get().encode("utf-8"))
                # insert masterpassword into database
                insert_password = """INSERT INTO masterpassword(password)
                            VALUES(?) """
                cursor.execute(insert_password, [hashedPassword])

                sql_command = 'INSERT INTO details (email) VALUES(?) '  # Query
                email = txt3.get()
                cursor.execute(sql_command, [email])

                db.commit()

                passwordVault()

        # else return label2 promting the user where they're going wrong
            else:
                lbl2.config(text="Password must have more than 6 characters")

        else:
            lbl2.config(text="Passwords do not Match")

    # save password
    btn = Button(window, text="Save password", command=savePassword)

    btn.pack(pady=10)


def loginScreen():
    # screen size
    window.geometry("325x250")

    lbl = Label(window, text="Enter Master password")
    lbl.config(anchor=CENTER)
    lbl.pack()

    # user input of master password
    txt = Entry(window, width=20, show="*")
    txt.pack()
    txt.focus()

    lbl2 = Label(window, text="Forgot your password? enter your email and secret below")
    lbl2.config(anchor=CENTER)
    lbl2.pack()

    lbl3 = Label(window, text="Email:")
    lbl3.config(anchor=CENTER)
    lbl3.pack()

    # user input of Email
    txt3 = Entry(window, width=20)
    txt3.pack()
    txt3.focus()

    lbl3 = Label(window, text="Secret:")
    lbl3.config(anchor=CENTER)
    lbl3.pack()
    # user input of secret
    txt2 = Entry(window, width=20)
    txt2.pack()
    txt2.focus()

    # label for incorrect password
    lbl1 = Label(window)
    lbl1.pack()

    # Add Shared secret
    #get masterpassword from db to be checked against the input masterpassword
    def getMasterPassword():
        checkHashedPassword = hashPassword(txt.get().encode("utf-8"))
        cursor.execute("SELECT * FROM masterpassword WHERE id = 1 AND password = ?", [(checkHashedPassword)])
        print(checkHashedPassword)
        return cursor.fetchall()

    # check if password is correct or incorrect
    def checkPassword():
        match = getMasterPassword()
        print(match)
        if match:
            passwordVault()
        #if no match, delete attempt and print prompt
        else:
            txt.delete(0, "end")
            lbl1.config(text="Password incorrect")

    btn = Button(window, text="Forgot Password")
    btn.pack(pady=10)
    # submit button
    btn = Button(window, text="Submit", command=checkPassword)
    btn.pack(pady=10)


def passwordVault():
    for widget in window.winfo_children():
        widget.destroy()

    def addEntry():
        text1 = "Website"
        text2 = "Username"
        text3 = "Password"

        website = popUp(text1)
        username = popUp(text2)
        password = popUp(text3)
        # insert entry into vault
        insert_fields = """INSERT INTO vault(website,username,password)
                    VALUES(?, ?, ?)"""

        cursor.execute(insert_fields, (website, username, password))
        db.commit()

        passwordVault()
    # remove entries from vault and db
    def removeEntry(input):
        cursor.execute("DELETE FROM vault WHERE id = ?", (input,))
        db.commit()

        passwordVault()
    # run the password generator
    def run():
        os.system("pwGenerator.py")
    # on exit encrypt the db
    def exit():
        os.system("EncryptionMethod.py")
        window.destroy()
    # 60 second timeout
    t = Timer(60, exit)
    t.start()  # after 60 seconds, application will exit

    window.geometry("800x350")  # make pretty
    window.configure(bg='light grey')

    # lbl = Label(window, text="password vault")
    # lbl.grid(column=0)
    # buttons for within vault
    lf = LabelFrame(window, text="Select password length")
    lf.grid(pady=20, row=1)

    btn = Button(window, text="Exit Securely",bg = 'light grey', command=exit)
    btn.grid(column=2, row=1)
    btn = Button(window, text="Add Entry",bg = 'light grey', command=addEntry)
    btn.grid(column=1, row=1)
    btn = Button(window, text="Generate",bg = 'light grey', command=run)
    btn.grid(column=0, row=1)
    # labels for input of website, username, and passwords
    lbl = Label(window,bg = 'light grey', text="Website", font = "Verdana 13 underline", bd=10)
    lbl.grid(row=2, column=0, padx=80)
    lbl = Label(window,bg = 'light grey', text="Username", font = "Verdana 13 underline")
    lbl.grid(row=2, column=1, padx=80)
    lbl = Label(window,bg = 'light grey', text="password", font = "Verdana 13 underline")
    lbl.grid(row=2, column=2, padx=80)
    # plots the users data to the page
    cursor.execute("SELECT * FROM vault")
    if (cursor.fetchall() != None):
        i = 0
        while True:
            cursor.execute("SELECT * FROM vault")
            array = cursor.fetchall()

            lbl1 = Label(window, text=(array[i][2]), font=(12),bg = 'light grey')
            lbl1.grid(column=0, row=i + 3)
            lbl1 = Label(window, text=(array[i][1]), font=(12),bg = 'light grey')
            lbl1.grid(column=1, row=i + 3)
            lbl1 = Label(window, text=(array[i][3]), font=(12),bg = 'light grey')
            lbl1.grid(column=2, row=i + 3)
            # delete entry button
            btn = Button(window, text="Delete", command=partial(removeEntry, array[i][0]))
            btn.grid(column=3, row=i + 3, pady=10)

            i = i + 1

            cursor.execute("SELECT * FROM vault")
            if (len(cursor.fetchall()) <= i):
                break


cursor.execute("SELECT * FROM masterpassword")
if cursor.fetchall():
    loginScreen()
else:
    firstScreen()
window.mainloop()
