from cryptography.fernet import Fernet

file = open("key.key", "rb")
key = file.read()
file.close()

# open the file to decrypt
with open("password_vault.db", "rb") as f:
    data = f.read()

    fernet = Fernet(key)
    encrypted = fernet.decrypt(data)

# write the decrypted file
with open("password_vault.db", "wb") as f:
    f.write(encrypted)
