import cryptography
from cryptography import fernet

from cryptography.fernet import Fernet

key = Fernet.generate_key()
print(key)
# uncomment to write key to file
#file = open ('key.key', 'wb')
#file.write(key)
# get the key from the file
file = open("key.key", "rb")
key = file.read()
file.close()

# open the file to encrypt
with open("password_vault.db", "rb") as f:
   data = f.read()

fernet = Fernet(key)
encrypted = fernet.encrypt(data)

# write the encrypted file
with open("password_vault.db", "wb") as f:
    f.write(encrypted)
